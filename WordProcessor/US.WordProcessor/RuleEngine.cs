﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using US.WordProcessor.Internal;

namespace US.WordProcessor
{
   public static class RuleEngine 
   {
      internal static List<Correction> Execute(Sentence sentence, Definition prev, Definition cur, Definition next, string curWord, IRule[] rules)
      {
         var corrections = new List<Correction>();
         foreach(var rule in rules)
         {
            corrections.Add(rule.Execute(sentence, prev, cur, next, curWord));
         }
         return corrections.Where(c => c != null).ToList();
      }
   }
}
