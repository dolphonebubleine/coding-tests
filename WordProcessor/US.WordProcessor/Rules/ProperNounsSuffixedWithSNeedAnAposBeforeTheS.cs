﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using US.WordProcessor.Internal;

namespace US.WordProcessor.Rules
{
   internal class ProperNounsSuffixedWithSNeedAnAposBeforeTheS : IRule
   {
      public Correction Execute(Sentence sentence, Definition prev, Definition cur, Definition next, string curWord)
      {
         var curIsProperNoun = cur.Type == WordType.ProperNoun;
         var curIsSuffixedWithS = cur.Suffix == "s";
         if(curIsProperNoun && curIsSuffixedWithS)
         {
            var missingApostropheBeforeTheS = !curWord.EndsWith("'s");
            if (missingApostropheBeforeTheS)
            {
               return new Correction(CorrectionType.OwnershipByAProperNoun, sentence.ToString(), curWord);
            }
         }
         return null;
      }
   }
}
