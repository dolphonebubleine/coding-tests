﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using US.WordProcessor.Internal;

namespace US.WordProcessor.Rules
{
   internal class RegularNounsDoNotNeedAnApostrophe : IRule
   {
      public Correction Execute(Sentence sentence, Definition prev, Definition cur, Definition next, string curWord)
      {
         var curIsRegularNoun = cur.Type == WordType.Noun;
         var curIsSuffixedWithS = cur.Suffix == "s";
         if (curIsRegularNoun && curIsSuffixedWithS)
         {
            var hasApostropheBeforeTheS = curWord.EndsWith("'s");
            if (hasApostropheBeforeTheS)
            {
               return new Correction(CorrectionType.IncorrectNounApostrophe, sentence.ToString(), curWord);
            }
         }
         return null;
      }
   }
}
