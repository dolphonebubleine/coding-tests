﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using US.WordProcessor.Internal;

namespace US.WordProcessor.Rules
{
   internal class WontNeedsAnApostrophe : IRule
   {
      public Correction Execute(Sentence sentence, Definition prev, Definition cur, Definition next, string curWord)
      {
         var isWontMissingApostrophe = curWord.ToLower() == "wont";
         if (isWontMissingApostrophe)
         {
            return new Correction(CorrectionType.MissingContractionApostrophe, sentence.ToString(), curWord);
         }
         return null;
      }
   }
}
