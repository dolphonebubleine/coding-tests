﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using US.WordProcessor.Internal;

namespace US.WordProcessor
{
   internal interface IRule
   {
      Correction Execute(Sentence sentence, Definition prev, Definition cur, Definition next, string curWord);
   }
}
