﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using US.WordProcessor.Internal;
using US.WordProcessor.Rules;

namespace US.WordProcessor
{   
   internal class CorrectionFinder 
      : ICorrectionFinder
   {                  
      public IEnumerable<Correction> Find(Paragraph paragraph)
      {
         var corrections = new List<Correction>();
         GetDefinition(paragraph, (sentence, prev, cur, next, curWord) =>
            corrections.AddRange(
               RuleEngine.Execute(sentence, prev, cur, next, curWord, new IRule[] {
                  new ProperNounsSuffixedWithSFollowedByANounNeedAnApostropheBeforeTheS(),
                  new ProperNounsSuffixedWithSPrecededByIsNeedAnApostropheBeforeTheS(),
                  new IsNeedsAnApostrophe(),
                  new WontNeedsAnApostrophe(),
                  new DoesntNeedsAnApostrophe(),
                  new ProperNounsSuffixedWithSNeedAnAposBeforeTheS(),
                  new RegularNounsDoNotNeedAnApostrophe()
               })
            )
         );
         return corrections
            .GroupBy(c => new { c.Type, c.Sentence, c.Word })
            .Select(g => new Correction(g.Key.Type, g.Key.Sentence, g.Key.Word));
      }

      public void GetDefinition(Paragraph paragraph, Action<Sentence, Definition, Definition, Definition, string> procesDefinition)
      {
         var dictionary = new US.WordProcessor.Internal.Dictionary();
         foreach (var sentence in paragraph)
         {
            var sentenceReader = new SentenceReader(sentence);
            var definitionReader = new DefinitionReader(dictionary, sentenceReader);
            while (definitionReader.MoveNext())
            {
               procesDefinition(sentence, definitionReader.PreviousDefinition, definitionReader.CurrentDefinition, definitionReader.NextDefinition, sentenceReader.Current);
            }
         }
      }
   }   
}
